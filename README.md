# symmetric_rdv

These notebooks provide an implementation of the Markov strategies with lookahead
for the problem of Symmetric rendez-vous on the line problem, as described in 

[Competitive Strategies for Symmetric Rendezvous on the Line](https://doi.org/10.1137/1.9781611977073.16)\
Max Klimm, Guillaume Sagnol, Martin Skutella, and Khai Van Tran\
Proceedings of the 2022 Annual ACM-SIAM Symposium on Discrete Algorithms (SODA). 2022, 329-347


They contain scripts allowing the community to reproduce the competitive ratios presented in
the above article, and in a not-yet published follow-up paper.

## Dependencies
The scripts require Python 3 and the following packages:

numpy\
scipy\
json

## Usage

* The notebook RDV.ipynb contains a class `LookAheadStrategy` used to store the data of a "Markov strategy with lookahead"
  (or without lookahead if L=1), and to compute the competitive ratios
  It also contains a class `AgentInformation` used to store the internal memory of each agent in Monte-Carlo simulations
  
* The folder `/optima` contains the best-found parameters for each pair of $h$ and $L$ (history length and maximum lunge size)
  
* The notebook `RDV_examples.ipynb` gives examples of how to use the above classes. In particular, a strategy with
  the optimized parameters stored in `/optima` can be loaded using the option `optimized=True`:
  
``` python
look_ahead = LookAheadStrategy(h=7, L=7, optimized=True)
print(look_ahead.competitive_ratio())
```

* The notebook `test_RDV.ipynb` contains a few unit tests. It also tests that the strategy parameters stored in the
  folder `/optima` yield the same competitive ratios as the ones stored in the `optima/*.json` files, and that
  the competitive ratio computed with the formula from the paper is consistent with the result of Monte-Carlo simulations.
  
* The notebook `param_optimization.ipynb` contains a class `Optimizer` that was used to optimize the strategy parameters. It
  implements a variant of the Pairwise Frank-Wolfe algorithm, with a line-search to re-optimize the scaling parameter `alpha`
  at regular intervals. An example of usage of this optimization script is provided at the end of the notebook.
  
